cmake_minimum_required(VERSION 2.8.11)
project(dbus_fun)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)
find_package(Qt5DBus REQUIRED)

set(dbus_fun_SRC
  src/main.cpp
  src/dbus_fun.cpp
)

#create the adaptor class
qt5_add_dbus_adaptor(dbus_fun_SRC src/my.dbus.dbus_fun.xml dbus_fun.h dbus_fun)
qt5_add_dbus_interface(dbus_fun_SRC src/my.dbus.dbus_fun.xml dbus_fun_interface)


include_directories(src/ ${CMAKE_CURRENT_BINARY_DIR})



# Create code from a list of Qt designer ui files.
#set(CMAKE_AUTOUIC ON) # use this if you have CMake 3.x instead of the following
qt5_wrap_ui(dbus_fun_SRC src/dbus_fun.ui)

# Tell CMake to create the helloworld executable
add_executable(dbus_fun ${dbus_fun_SRC})

# Use the Widgets module from Qt 5.
target_link_libraries(dbus_fun Qt5::Widgets Qt5::DBus)
