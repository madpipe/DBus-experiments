#ifndef DBUS_FUN_H
#define DBUS_FUN_H

#include <QMainWindow>

namespace Ui {
class dbus_fun;
}

class dbus_fun : public QMainWindow
{
    Q_OBJECT

public:
    explicit dbus_fun(QWidget *parent = 0);
    ~dbus_fun();
    Q_INVOKABLE void setLabelText(const QString& message); // in can be invoked through QMetaObject system
private Q_SLOTS:
    void buttonClicked();
private:
    Ui::dbus_fun *ui;
};

#endif // DBUS_FUN_H
