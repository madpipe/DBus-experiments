#include "dbus_fun.h"
#include <QApplication>
#include <QDBusConnection>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    dbus_fun w;
    w.show();

    QDBusConnection::sessionBus().registerService("my.dbus.testapp");
    
    return app.exec();
}
