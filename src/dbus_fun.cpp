#include "dbus_fun.h"
#include "ui_dbus_fun.h"
#include <QDebug>
#include <QDBusConnection>
#include "dbus_funadaptor.h"
#include "dbus_fun_interface.h"

dbus_fun::dbus_fun(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::dbus_fun)
{
    ui->setupUi(this);
    QDBusConnection::sessionBus().registerObject("/", this);
    new Dbus_funAdaptor(this);
    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(buttonClicked()));
}

dbus_fun::~dbus_fun()
{
    delete ui;
}

void dbus_fun::setLabelText(const QString& message)
{
  ui->label->setText(message);
}

void dbus_fun::buttonClicked()
{
    my::dbus::dbus_fun iFace("my.dbus.testapp", "/", QDBusConnection::sessionBus());
    if(iFace.isValid())
    {
	iFace.setLabelText("Hello from myself through DBus!!!!!!");
    }
    
//     QDBusMessage msg = QDBusMessage::createMethodCall("my.dbus.testapp", "/", "my.dbus.dbus_fun", "setLabelText");
//     msg.setArguments(QVariantList()<<"Hello from myself through DBus!!!!!!");
//     QDBusConnection::sessionBus().asyncCall(msg);
}
